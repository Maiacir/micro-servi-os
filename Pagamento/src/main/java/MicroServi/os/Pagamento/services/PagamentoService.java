package MicroServi.os.Pagamento.services;

import MicroServi.os.Pagamento.clients.CartaoClients;
import MicroServi.os.Pagamento.models.Cartao;
import MicroServi.os.Pagamento.models.Pagamento;
import MicroServi.os.Pagamento.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    PagamentoRepository pagamentoRepository;

    @Autowired
    CartaoClients cartaoService;

    public Pagamento salvarPagamento(Pagamento pagamento) {
        Cartao cartao = cartaoService.buscarCartaoPorId(pagamento.getCartaoId());

        pagamento.setCartaoId(cartao.getId());

        return pagamentoRepository.save(pagamento);
    }

    public List<Pagamento> buscarTodosCartoes(Long cartaoId) {
        return pagamentoRepository.findAllByCartao_id(cartaoId);
    }
}
