package MicroServi.os.Pagamento.controllers;

import MicroServi.os.Pagamento.models.Pagamento;
import MicroServi.os.Pagamento.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PagamentoController {

    @Autowired
    PagamentoService pagamentoService;

    @PostMapping("/pagamento")
    public Pagamento salvarPagamento(@RequestBody Pagamento pagamento) {
        return pagamentoService.salvarPagamento(pagamento);
    }

    @GetMapping("/pagamentos/{cartaoId")
    public List<Pagamento> buscarTodosCartoes(@PathVariable Long cartaoId) {
        return pagamentoService.buscarTodosCartoes(cartaoId);
    }

}
