package MicroServi.os.Pagamento.repositories;

import MicroServi.os.Pagamento.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PagamentoRepository extends CrudRepository<Pagamento, Long> {

    List<Pagamento> findAllByCartao_id(Long cartaoId);
}
