package MicroServi.os.Pagamento.clients;

import MicroServi.os.Pagamento.models.Cartao;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cartao", configuration = CartaoClientsConfiguration.class)
public interface CartaoClients {

    @GetMapping("/cartao/id/{id}")
    Cartao buscarCartaoPorId(@PathVariable Long id);
}
