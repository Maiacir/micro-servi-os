package MicroServi.os.Pagamento.clients;

import MicroServi.os.Pagamento.models.Cartao;

public class CartaoClientFallBack implements CartaoClients {
    @Override
    public Cartao buscarCartaoPorId(Long id) {
        Cartao cartaoFallBack = new Cartao();

        cartaoFallBack.setId((long) 01);
        cartaoFallBack.setClienteId((long) 000111222);
        cartaoFallBack.setAtivo(false);

        return null;
    }
}
