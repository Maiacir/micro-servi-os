package MicroServi.os.Pagamento.clients;

import MicroServi.os.Pagamento.exceptions.CartaoInvalidException;
import feign.Response;
import feign.codec.ErrorDecoder;

import java.awt.geom.RectangularShape;

public class CartaoClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder= new Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 404) {
            throw new CartaoInvalidException();
        } else {
            return errorDecoder.decode(s, response);
        }
    }
}
