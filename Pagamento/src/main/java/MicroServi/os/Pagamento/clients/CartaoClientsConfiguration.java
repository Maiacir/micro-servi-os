package MicroServi.os.Pagamento.clients;

import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class CartaoClientsConfiguration {

    @Bean
    public ErrorDecoder buscarErrorDecoder() {
        return new CartaoClientDecoder();
    }
}
