package MicroServi.os.Pagamento.models;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table
public class Pagamento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String descricao;

    @Column
    private BigDecimal valor;

    @Column
    private Long cartaoId;

    public Pagamento() {
    }

    public Pagamento(Long id, String descricao, BigDecimal valor, Long cartaoId) {
        this.id = id;
        this.descricao = descricao;
        this.valor = valor;
        this.cartaoId = cartaoId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public Long getCartaoId() {
        return cartaoId;
    }

    public void setCartaoId(Long cartaoId) {
        this.cartaoId = cartaoId;
    }
}
