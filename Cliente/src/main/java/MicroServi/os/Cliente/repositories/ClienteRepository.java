package MicroServi.os.Cliente.repositories;

import MicroServi.os.Cliente.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Long> {
}
