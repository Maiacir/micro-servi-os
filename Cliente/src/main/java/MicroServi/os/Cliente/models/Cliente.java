package MicroServi.os.Cliente.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(min=8, max=100, message= "Nome do cliente deve ter entre 8 a 100 caracteres")
    private String nome;

    public Cliente() {
    }

    public Cliente(Long id, @NotBlank @Size(min = 8, max = 100, message = "Nome do cliente deve ter entre 8 a 100 caracteres") String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
