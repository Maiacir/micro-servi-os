package MicroServi.os.Cliente.models.dto;

public class SalvarClienteRequest {

    private String nome;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
