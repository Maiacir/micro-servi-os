package MicroServi.os.Cliente.models.dto;

import MicroServi.os.Cliente.models.Cliente;
import org.springframework.stereotype.Component;

@Component
public class ClienteMapper {

    public Cliente toCliente(SalvarClienteRequest salvarClienteRequest) {
        Cliente cliente = new Cliente();
        cliente.setNome(salvarClienteRequest.getNome());
        return cliente;
    }
}
