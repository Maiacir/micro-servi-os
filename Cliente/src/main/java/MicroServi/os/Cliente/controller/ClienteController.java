package MicroServi.os.Cliente.controller;

import MicroServi.os.Cliente.models.Cliente;
import MicroServi.os.Cliente.models.dto.ClienteMapper;
import MicroServi.os.Cliente.models.dto.SalvarClienteRequest;
import MicroServi.os.Cliente.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/clientes")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private ClienteMapper mapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente salvarCliente (@RequestBody SalvarClienteRequest salvarClienteRequest) {
        Cliente cliente = mapper.toCliente(salvarClienteRequest);

        cliente = clienteService.salvarCliente(cliente);

        return cliente;
    }

    @GetMapping("/{id}")
    public Cliente buscarPorId(@PathVariable Long id) {
        return clienteService.buscarPorId(id);
    }

}
