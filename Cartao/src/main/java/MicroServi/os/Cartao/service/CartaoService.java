package MicroServi.os.Cartao.service;

import MicroServi.os.Cartao.clients.ClienteClient;
import MicroServi.os.Cartao.exceptions.CartaoNotFoundException;
import MicroServi.os.Cartao.models.Cartao;
import MicroServi.os.Cartao.models.Cliente;
import MicroServi.os.Cartao.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteClient clienteClient;

    public Cartao salvarCartao(Cartao cartao) {
        Cliente cliente = clienteClient.buscarClientePorId(cartao.getClienteId());

        cartao.setClienteId(cliente.getId());
        cartao.setAtivo(false);

        return cartaoRepository.save(cartao);
    }

    public Cartao atualizarCartao(Cartao cartao) {
        Cartao databaseCartao = buscarPorNumero(cartao.getNumero());

        databaseCartao.setAtivo(cartao.getAtivo());

        return cartaoRepository.save(databaseCartao);
    }

    public Cartao buscarPorNumero(String numero) {
        Optional<Cartao> porId = cartaoRepository.findByNumero(numero);

        if (!porId.isPresent()) {
            throw new CartaoNotFoundException();
        }
        return porId.get();
    }

    public Cartao buscarPorId(Long id) {
        Optional<Cartao> porId = cartaoRepository.findById(id);

        if(!porId.isPresent()) {
            throw new CartaoNotFoundException();
        }
        return porId.get();
    }

}
