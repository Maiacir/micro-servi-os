package MicroServi.os.Cartao.clients;

import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class ClienteClientConfiguration {

    @Bean
    public ErrorDecoder buscarErroDecoder() {
        return new ClienteClientDecoder();
    }
}
