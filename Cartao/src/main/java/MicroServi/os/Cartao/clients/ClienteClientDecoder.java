package MicroServi.os.Cartao.clients;

import MicroServi.os.Cartao.exceptions.ClienteInvalidException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class ClienteClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder= new Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 404) {
            throw new ClienteInvalidException();
        } else {
            return errorDecoder.decode(s, response);
        }
    }
}
