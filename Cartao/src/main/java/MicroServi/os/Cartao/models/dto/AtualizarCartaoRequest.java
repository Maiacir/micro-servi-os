package MicroServi.os.Cartao.models.dto;

public class AtualizarCartaoRequest {

    private String numero;

    private Boolean ativo;

    public AtualizarCartaoRequest() {
    }

    public AtualizarCartaoRequest(String numero, Boolean ativo) {
        this.numero = numero;
        this.ativo = ativo;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
}
