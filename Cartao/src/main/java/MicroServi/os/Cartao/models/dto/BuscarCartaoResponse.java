package MicroServi.os.Cartao.models.dto;

public class BuscarCartaoResponse {

    private Long id;

    private String numero;

    private Long clienteId;

    public BuscarCartaoResponse() {
    }

    public BuscarCartaoResponse(Long id, String numero, Long clienteId) {
        this.id = id;
        this.numero = numero;
        this.clienteId = clienteId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Long getClienteId() {
        return clienteId;
    }

    public void setClienteId(Long clienteId) {
        this.clienteId = clienteId;
    }
}
