package MicroServi.os.Cartao.models.dto;

import MicroServi.os.Cartao.models.Cartao;
import MicroServi.os.Cartao.models.Cliente;

public class CartaoMapper {

    public Cartao toCartao(SalvarCartaoRequest salvarCartaoRequest) {
        Cartao cartao = new Cartao();
        cartao.setNumero(salvarCartaoRequest.getNumero());
        cartao.setClienteId(salvarCartaoRequest.getClienteId());

        return cartao;
    }

    public SalvarCartaoResponse toSalvarCartaoResponse(Cartao cartao) {
        SalvarCartaoResponse salvarCartaoResponse = new SalvarCartaoResponse();

        salvarCartaoResponse.setId(cartao.getId());
        salvarCartaoResponse.setNumero(cartao.getNumero());
        salvarCartaoResponse.setClienteId(cartao.getClienteId());
        salvarCartaoResponse.setAtivo(cartao.getAtivo());

        return salvarCartaoResponse;
    }

    public Cartao toCartao(AtualizarCartaoRequest atualizarCartaoRequest) {
        Cartao cartao = new Cartao();

        cartao.setNumero(atualizarCartaoRequest.getNumero());
        cartao.setAtivo(atualizarCartaoRequest.getAtivo());

        return cartao;
    }


    public AtualizarCartaoResponse toAtualizarCartaoResponse(Cartao cartao) {
        AtualizarCartaoResponse atualizarCartaoResponse = new AtualizarCartaoResponse();

        atualizarCartaoResponse.setId(cartao.getId());
        atualizarCartaoResponse.setNumero(cartao.getNumero());
        atualizarCartaoResponse.setClienteId(cartao.getClienteId());
        atualizarCartaoResponse.setAtivo(cartao.getAtivo());

        return atualizarCartaoResponse;
    }

    public BuscarCartaoResponse toBuscarCartaoResponse(Cartao cartao) {
        BuscarCartaoResponse buscarCartaoResponse = new BuscarCartaoResponse();

        buscarCartaoResponse.setId(cartao.getId());
        buscarCartaoResponse.setNumero(cartao.getNumero());
        buscarCartaoResponse.setClienteId(cartao.getClienteId());

        return buscarCartaoResponse;
    }

}
