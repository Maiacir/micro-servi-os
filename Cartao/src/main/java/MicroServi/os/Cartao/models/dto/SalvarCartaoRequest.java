package MicroServi.os.Cartao.models.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class SalvarCartaoRequest {

    @NotBlank
    @Size(min = 10)
    private String numero;

    @NotNull
    private Long clienteId;

    public SalvarCartaoRequest() {
    }

    public SalvarCartaoRequest(@NotBlank @Size(min = 10) String numero, @NotNull Long clienteId) {
        this.numero = numero;
        this.clienteId = clienteId;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Long getClienteId() {
        return clienteId;
    }

    public void setClienteId(Long clienteId) {
        this.clienteId = clienteId;
    }
}
