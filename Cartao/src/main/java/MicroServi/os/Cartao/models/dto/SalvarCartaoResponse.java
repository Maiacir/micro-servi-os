package MicroServi.os.Cartao.models.dto;

public class SalvarCartaoResponse {

    private Long id;

    private String numero;

    private Long clienteId;

    private Boolean ativo;

    public SalvarCartaoResponse() {
    }

    public SalvarCartaoResponse(Long id, String numero, Long clienteId, Boolean ativo) {
        this.id = id;
        this.numero = numero;
        this.clienteId = clienteId;
        this.ativo = ativo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Long getClienteId() {
        return clienteId;
    }

    public void setClienteId(Long clienteId) {
        this.clienteId = clienteId;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
}
