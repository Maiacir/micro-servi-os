package MicroServi.os.Cartao.controllers;

import MicroServi.os.Cartao.models.Cartao;
import MicroServi.os.Cartao.models.dto.*;
import MicroServi.os.Cartao.service.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartoes")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @Autowired
    private CartaoMapper mapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public SalvarCartaoResponse salvarCartao(@Valid @RequestBody SalvarCartaoRequest salvarCartaoRequest) {
        Cartao cartao = mapper.toCartao(salvarCartaoRequest);

        cartao = cartaoService.salvarCartao(cartao);

        return mapper.toSalvarCartaoResponse(cartao);
    }

    @PatchMapping("/{numero}")
    @ResponseStatus(HttpStatus.OK)
    public AtualizarCartaoResponse atualizarCartao(@PathVariable String numero, @RequestBody AtualizarCartaoRequest atualizarCartaoRequest) {
        atualizarCartaoRequest.setNumero(numero);
        Cartao cartao = mapper.toCartao(atualizarCartaoRequest);

        cartao = cartaoService.atualizarCartao(cartao);

        return mapper.toAtualizarCartaoResponse(cartao);

    }


    @GetMapping("/{numero}")
    @ResponseStatus(HttpStatus.OK)
    public BuscarCartaoResponse buscarPorNumero(@PathVariable String numero) {
        Cartao cartao = cartaoService.buscarPorNumero(numero);
        return mapper.toBuscarCartaoResponse(cartao);
    }

    @GetMapping("/id/{id}")
    @ResponseStatus(HttpStatus.OK)
    public BuscarCartaoResponse buscarCartaoResponse(@PathVariable Long id) {
        Cartao cartao = cartaoService.buscarPorId(id);

        return mapper.toBuscarCartaoResponse(cartao);
    }
}
